import React, { Component, PureComponent } from 'react';
import { TweenLite, TweenMax, TimelineMax} from 'gsap';
import GSAP from 'react-gsap-enhancer';

class FeeCounter extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      direction: '',
      currentValue: this.props.currentValue,
      x: 0,
      y: 0,
      targetValue: this.props.currentValue
    }

    this.increaseAnimation = this.increaseAnimation.bind(this)
    this.decreaseAnimation = this.decreaseAnimation.bind(this)
  } 

  increaseAnimation({target}) {
    let counter = target.find({name: 'counter'})
    return new TimelineMax({repeat: 0})
                    .to(counter, 0.3, {x: '-=100'})
                    .to(counter,   0, {x: '+=200'})
                    .add(function() {
                      let val = this.calculateValue(this.state.currentValue + 1)
                      this.setState({
                        direction: 'increase',
                        currentValue: val,
                        x: this.state.x,
                        y: this.state.y,
                      });
                    }.bind(this))
                    .to(counter, 0.3, {x: '-=100'})
                    .set(counter, {x: 0});
  
  }

  decreaseAnimation({target}) {
    let counter = target.find({name: 'counter'})
    return new TimelineMax({repeat: 0})
                    .to(counter, 0.3, {x: '+=100'})
                    .to(counter,   0, {x: '-=200'})
                    .add(function() {
                      let val = this.calculateValue(this.state.currentValue - 1);
                      this.setState({
                        direction: 'decrease',
                        currentValue: val,
                        x: this.state.x,
                        y: this.state.y
                      });
                    }.bind(this))
                    .to(counter, 0.3, {x: '+=100'})
                    .set(counter, {x: 0});
  }


  increase = () => {
    this.addAnimation(this.increaseAnimation);
  }

  decrease = () => {
    this.addAnimation(this.decreaseAnimation);
  }

  calculateValue(val) {
    if (val > this.props.max) {
      val = this.props.min;
    }

    if (val < this.props.min) {
      val = this.props.max;
    }

    return val;
  }

  render() {
    let {x, y} = this.state;
    let style = {
      transform: `translate(${x}px, ${y}px)`,
      width: 30, 
      'text-align': 'center'
    }
  
    let rootStyle = {
      overflow: 'hidden',
      width: 30,
      backgroundColor: 'pink'
    }
    
    return (
      <div>  
        <button onClick={this.decrease} />
        <div style={rootStyle}>
          <div name='counter' style={style}>{this.state.currentValue}</div>
        </div>
        <button onClick={this.increase} />
      </div>
    )   
  }
}

export default GSAP()(FeeCounter)
